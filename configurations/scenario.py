scenario = {
    'input': {
        'input_type': 'device',
        'device': {
            'name': 'OpenBCIGanglion',
            'OpenBCI_Ganglion': {
                'mac_address': 'e2:31:cf:da:84:20'
            } 
        }
    },
    'output': {
        'output_type': 'file',
        'file': {
            'directory': 'var',
        }
    }
}