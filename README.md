# MindLAB Pulsepype
![alt text](./docs/pulsepype_logo.png "Pulsepype logo")   

Pulsepype is a software that route brain data from an input to an output.  

## Pre-requisite (How-to install)
How-to setup the environment: [how-to](docs/how-to.md)   

## Usage

```
python main.py
```

## Pipe: inputs - outputs
A pipe made of brain-data input and output is defined in ```scenario.py```
configuration file.

## Credits
The driver for acquiring brain-data from OpenBCI Ganglion boards is freely
based on [https://github.com/OpenBCI/pyOpenBCI](pyOpenBCI).   
The OpenBCI Ganglion driver module used here 
```./lib/inputs/ganglion.py``` 
has decoupled *board connection* and *data-stream* capabilities.   
