# This project how-to

## System
Ubuntu 18.04

## Setup

```
# Conda
conda update conda
conda create --name MindLAB_pulsepype
conda activate MindLAB_pulsepype
conda install python=3.8.0

# Python libs
pip install numpy pyserial bitstring xmltodict requests bluepy

# enable bluepy-helper functionalities 
sudo setcap 'cap_net_raw,cap_net_admin+eip' ~/anaconda3/envs/MindLAB_pulsepype/lib/python3.8/site-packages/bluepy/bluepy-helper

```