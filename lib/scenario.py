import sys
import os
import logging

from lib.inputs.ganglion import OpenBCIGanglion
from lib.outputs.file import file

from configurations.logger import logger as cfg_logger

class scenario():
    """ Class for managing scenarios """
    
    def __init__(self, scenario_in, scenario_out):
        
        self.init_log()
        
        self.scenario_in = scenario_in
        self.handle_in (scenario_in)
        
        self.scenario_out = scenario_out
        self.handle_out (scenario_out)
        
        self.data_flow ()
    
    
    def init_log (self):
        filepath = cfg_logger['filepath']
        filepath_split = os.path.split (filepath) 
        if not os.path.isdir (filepath_split[0]):
            os.makedirs (filepath_split[0])
        logging.basicConfig (level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    filename=filepath,
                    filemode='a')
        logger = logging.getLogger ('pulsepype.scenario')
        logger.info ('pulsepype.scenario is running')
    
    
    def handle_in (self, scenario_in):
        input_type = scenario_in['input_type']
        getattr (self, 'handle_in_' + input_type) (scenario_in)
    
    def handle_in_device (self, scenario_in):
        device_name = scenario_in['device']['name']
        getattr (self, 'handle_in_device_' + device_name) (scenario_in)
        
    def handle_in_device_OpenBCIGanglion (self, scenario_in):
        mac_address = scenario_in['device']['OpenBCI_Ganglion']['mac_address']
        board = OpenBCIGanglion ()
        try:
            logger = logging.getLogger ('pulsepype.scenario')
            logger.info ('Connecting to Ganglion ...')
            board.connect (mac_address)
            logger.info ('Ganglion board connected')
            self.scenario_in['object'] = board
        except Exception as e:
                print ("Pipe element has failed: \n" + str (e))
                sys.exit(0)


    def handle_out (self, scenario_out):
        output_type = scenario_out['output_type']
        getattr (self, 'handle_out_' + output_type) (scenario_out)
        
    def handle_out_file (self, scenario_out):
        try:
            logger = logging.getLogger ('pulsepype.scenario')
            logger.info ('Opening file writing ...')
            file_writer = file (scenario_out['file']['directory'])
            logger.info ('File writer opened')
            self.scenario_out['object'] = file_writer.file
        except Exception as e:
            print ("Pipe element has failed: \n" + str (e))
            sys.exit(0)
        
    def data_flow (self):
        pass