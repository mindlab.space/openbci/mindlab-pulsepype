import os
from datetime import datetime

from configurations.scenario import scenario as cfg_scenario

class file():
    
    def __init__ (self, directory):
        if not os.path.exists (directory):
            os.makedirs (directory)
        dateTimeObj = datetime.now()
        filename = str (dateTimeObj.year) + '-' + str (dateTimeObj.month) + '-'
        filename += str (dateTimeObj.day) 
        filename += '.txt'
        filepath = directory + '/' + filename
        file = open (filepath,'a')
        self.file = file
        
    def write_file_raw (self, sample=None):
        
        file = open (self.filepath,'a')
        if sample is not None:
            file.write ("%s\n" % str(sample.channels_data))
            