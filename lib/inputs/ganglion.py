import sys
import atexit
import numpy as np
import datetime
import warnings
from bitstring import BitArray

from bluepy.btle import Peripheral, Scanner, DefaultDelegate


# Ganglion board characteristics
SAMPLE_RATE = 200.0  # Hz (Bluetooth)
MCP3912_Vref = 1.2
MCP3912_gain = 1.0
scale_fac_uVolts_per_count = (MCP3912_Vref * 1000000) / (8388607.0 * MCP3912_gain * 1.5 * 51.0)
scale_fac_volts_per_count = (MCP3912_Vref * 1000000) / (8388607.0 * MCP3912_gain * 1.5 * 51.0)

# Bluetooth
# Service for communication, as per docs (see pyOpenBCI)
BLE_SERVICE = "fe84"
# Characteristics of interest
BLE_CHAR_RECEIVE = "2d30c082f39f4ce6923f3484ea480596"
BLE_CHAR_SEND = "2d30c083f39f4ce6923f3484ea480596"
BLE_CHAR_DISCONNECT = "2d30c084f39f4ce6923f3484ea480596"

class OpenBCIGanglion():
    """ Extend original pyOpenBCI.OpenBCIGanglion Class """
    
    def __init__(self, mac=None, max_packets_skipped=15):
        self.board_id = None
        self.max_packets_skipped = max_packets_skipped
        self.connected = False
        self.streaming = False
        self.board_type = 'Ganglion'
        
    def find_boards(self):
        """Finds and returns the mac addresses Ganglion boards found"""
        scanner = Scanner()
        devices = scanner.scan(5)
        if len(devices) < 1:
            raise OSError('No nearby devices found.')
        else:
            gang_macs = {}
            for dev in devices:
                for adtype, desc, value in dev.getScanData():
                    if desc == 'Complete Local Name' and value.startswith('Ganglion'):
                        gang_macs[value] = dev.addr
            return gang_macs
        
    def connect(self, mac_address):
        """Establishes connection with the specified Ganglion board."""
        self.ganglion = Peripheral(mac_address, 'random')
        self.service = self.ganglion.getServiceByUUID(BLE_SERVICE)
        self.char_read = self.service.getCharacteristics(BLE_CHAR_RECEIVE)[0]
        self.char_write = self.service.getCharacteristics(BLE_CHAR_SEND)[0]
        self.char_discon = self.service.getCharacteristics(BLE_CHAR_DISCONNECT)[0]
        self.ble_delegate = GanglionDelegate(self.max_packets_skipped)
        self.ganglion.setDelegate(self.ble_delegate)
        self.desc_notify = self.char_read.getDescriptors(forUUID=0x2902)[0]
        try:
            response = self.desc_notify.write(b"\x01")
            atexit.register(self.disconnect)
            self.connected = True
        except Exception as e:
            print("Something went wrong while trying to enable notification: " + str(e))
            sys.exit(2)
            
    def disconnect(self):
        """Disconnets from the Ganglion board."""
        if self.streaming:
            self.stop_stream()
        self.char_discon.write(b' ')
        self.ganglion.disconnect()
        
    def stop_stream(self):
        """Stops Ganglion Stream."""
        self.streaming = False
        self.write_command('s')
            
    def write_command(self, command):
        """Sends string command to the Ganglion board."""
        response = self.char_write.write(str.encode(command))
                        
    def start_stream(self, callback):
        """Start handling streaming data from the Ganglion board. Call a provided callback for every single sample that is processed."""
        if not self.streaming:
            self.streaming = True
            self.dropped_packets = 0
            self.write_command('b')

        if not isinstance(callback, list):
            callback = [callback]

        while self.streaming:
            try:
                self.ganglion.waitForNotifications(1./SAMPLE_RATE)
            except Exception as e:
                print(e)
                print('Something went wrong')
                sys.exit(1)

            samples = self.ble_delegate.getSamples()
            if samples:
                for sample in samples:
                    sample.channels_data = sample.channels_data * scale_fac_uVolts_per_count
                    for call in callback:
                        call(sample=sample)
                        
class GanglionDelegate(DefaultDelegate):
    """ Delegate Object used by bluepy. 
    Parses the Ganglion Data to return an OpenBCISample object.
    """
    def __init__(self, max_packets_skipped=15):

        DefaultDelegate.__init__(self)
        self.max_packets_skipped = max_packets_skipped
        self.last_values = [0, 0, 0, 0]
        self.last_id = -1
        self.samples = []
        self.start_time = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
        
    def handleNotification(self, cHandle, data):
        """Called when data is received. It parses the raw data from the Ganglion and returns an OpenBCISample object"""
        if len(data) < 1:
            warnings.warn('A packet should at least hold one byte...')
        self.parse_raw(data)
        
    def getSamples(self):
        """Returns the last OpenBCI Samples in the stack"""
        old_samples = self.samples
        self.samples = []
        return old_samples
    
    def parse_raw(self, raw_data):
        """Parses the data from the Cyton board into an OpenBCISample object."""
        start_byte = raw_data[0]
        bit_array = BitArray()
        self.checked_dropped(start_byte)

        if start_byte == 0:
            for byte in raw_data[1:13]:
                bit_array.append('0b{0:08b}'.format(byte))
                results = []
                # and split it into 24-bit chunks here
                for sub_array in bit_array.cut(24):
                    # calling ".int" interprets the value as signed 2's complement
                    results.append(sub_array.int)
                    self.last_values = np.array(results)
                    # print(self.last_values)
                    self.push_sample( [np.append(start_byte, self.last_values)])

        elif start_byte >=1 and start_byte <=100:
            for byte in raw_data[1:-1]:
                bit_array.append('0b{0:08b}'.format(byte))
                deltas = []
                for sub_array in bit_array.cut(18):
                    deltas.append(self.decompress_signed(sub_array))
                    delta1 , delta2 = np.array(deltas[:4]) , np.array(deltas[4:])
                    self.last_values1 = self.last_values - delta1
                    self.last_values = self.last_values1 - delta2
                    self.push_sample([self.last_values1, self.last_values])

        elif start_byte >=101 and start_byte <=200:
                for byte in raw_data[1:]:
                    bit_array.append('0b{0:08b}'.format(byte))
                deltas = []
                for sub_array in bit_array.cut(19):
                    deltas.append(self.decompress_signed(sub_array))
                delta1 , delta2 = np.array(deltas[:4]) , np.array(deltas[4:])
                self.last_values1 = self.last_values - delta1
                # print(self.last_values1)
                self.last_values = self.last_values1 - delta2
                # print(self.last_values)
                self.push_sample([np.append(start_byte,self.last_values1), np.append(start_byte,self.last_values)])

    def push_sample(self, data):
        """Creates a stack with the last ganglion Samples"""
        for data_arr in data:
            if len(data_arr) == 5:
                sample = OpenBCISample(data_arr[0], data_arr[1:], [], self.start_time, 'Ganglion')
                self.samples.append(sample)
                
    def checked_dropped(self, num):
        """Checks dropped packets"""
        if num not in [206, 207]:
            if self.last_id == 0 and num not in [1, 101]:
                if num > 100:
                    dropped = num - 100
                else:
                    dropped = num
            elif self.last_id == 0:
                dropped = 0
            elif self.last_id > num:
                dropped = 100 - abs(self.last_id - num)
            else:
                dropped = abs(self.last_id - num)
            if dropped > self.max_packets_skipped:
                print("Dropped %d packets...." % dropped)
            self.last_id = num
            
    def decompress_signed(self, bit_array):
        """Used to decrompress signed bit arrays."""
        result = bit_array.int
        if bit_array.endswith('0b1'):
            result -= 1
        return result