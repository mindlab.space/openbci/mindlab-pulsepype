import sys
import os
import logging

from lib.scenario import scenario

from configurations.scenario import scenario as cfg_scenario
from configurations.logger import logger as cfg_logger

def main ():
    
    init_log()
    
    scenario_input = cfg_scenario['input']
    scenario_output = cfg_scenario['output']
    scene = scenario (scenario_input, scenario_output)

def init_log():
    filepath = cfg_logger['filepath']
    filepath_split = os.path.split (filepath) 
    if not os.path.isdir (filepath_split[0]):
        os.makedirs (filepath_split[0])
    logging.basicConfig (level=logging.DEBUG,
                format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                filename=filepath,
                filemode='a')
    logger = logging.getLogger ('pulsepype.main')
    logger.info ('pulsepype.main is running')

if __name__ == "__main__":
    main ()